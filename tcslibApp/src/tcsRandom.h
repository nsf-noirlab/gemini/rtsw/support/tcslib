/*
*   FILENAME
*   tcsRandom.h
*
*   DESCRIPTION
*   Header file for tcsRandom.c which contains ex slalib routines used
*   to generate random numbers etc.
*
*/
/* *INDENT-OFF* */
/*
 * $Log: tcsRandom.h,v $
 * Revision 1.1.1.1  2005/06/20 09:54:23  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/20 09:22:19  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/17 14:22:51  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/17 10:24:05  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/14 15:13:15  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/14 11:26:21  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/13 15:21:58  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/09 14:59:13  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/08 16:04:57  ajf
 * Initial creation of the Gemini TCS3-14 repository
 *
 * Revision 1.1.1.1  1998/11/08 00:21:13  epics
 * Version 1-2 of the TCS as transferred from DRAL
 *
 * Revision 1.1  1997/02/10 17:10:57  pbt
 * Recreate in tcs subdirectory
 *
 * Revision 1.1  1996/05/23 20:33:42  cjm
 * Initial version
 *
 */
/* *INDENT-ON* */

#ifndef TCSRANDOMH
#define TCSRANDOMH

float tcsRandom( float seed) ;

float tcsGresid( float s ) ;

#endif
