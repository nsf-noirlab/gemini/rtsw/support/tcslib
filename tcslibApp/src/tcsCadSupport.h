/* *INDENT-OFF* */
/*
*   FILENAME
*   tcsCadSupport.h
*
*   DESCRIPTION
*   Declares CAD support routines
*
*/
/*
 * $Log:
 *
 */
/* *INDENT-ON* */

#ifndef TCSCADSUPPORT_H
#define TCSCADSUPPORT_H

#include <cadRecord.h>
#include <astLib.h>
#include "tcsSubsysCadSupport.h"
#include "tcsConstants.h"

char* tcsCsFrame( FRAMETYPE );
char* tcsCsOffCoord( OFFCOORD );

#endif
