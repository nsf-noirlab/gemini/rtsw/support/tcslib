/*
*   FILENAME
*   tcslib.h
*
*   DESCRIPTION
*   Header file for tcslib. It simply includes other header
*   files so that users of routines in tcslib.o don't have
*   to remember to include a whole bunch of other include files.
*   In particular, use of this header file does not require the
*   inclusion of tcsConstants.h which has ben found to cause
*   name space clashes in some subsystems.
*
*/
/* *INDENT-OFF* */
/*
 * $Log: tcslib.h,v $
 * Revision 1.1.1.1  2005/06/20 09:54:23  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/20 09:22:19  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/17 14:22:51  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/17 10:24:05  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/14 15:13:15  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/14 11:26:21  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/13 15:21:58  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/09 14:59:13  ajf
 * Initial creation of the Gemini TCS 3-14 repository
 *
 * Revision 1.1.1.1  2005/06/08 16:04:58  ajf
 * Initial creation of the Gemini TCS3-14 repository
 *
 * Revision 1.1.1.1  1998/11/08 00:21:14  epics
 * Version 1-2 of the TCS as transferred from DRAL
 *
 * Revision 1.1  1998/07/01 12:13:57  tcs
 * Initial version
 *
 *
 */
/* *INDENT-ON* */

#ifndef TCSLIBH 
#define TCSLIBH 

/* put definitions here */

#include "tcsSubsysCadSupport.h"
#include "tcsSubsysDecode.h"

#endif
